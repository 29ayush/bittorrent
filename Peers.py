# coding=utf-8
import bencode
import random
import requests
import struct,socket
from pubsub import pub
from libs import utils
import Peer

class Peers(object):
    def __init__(self, torrent,port=8090,proxies={}):
        self.torrent = torrent
        self.activepeers = []
        self.unchokedpeers = []
        self.
        self.port=port
        self.proxies=proxies
   
        # Events
        pub.subscribe(self.addPeer, 'PeersManager.newPeer')
        pub.subscribe(self.addUnchokedPeer, 'PeersManager.peerUnchoked')
        #pub.subscribe(self.handlePeerRequests, 'PeersManager.PeerRequestsPiece')
        #pub.subscribe(self.peersBitfield, 'PeersManager.updatePeersBitfield')
        
    def run(self):
        while True:
            self.startConnectionToPeers()
            read = [p.socket for p in self.peers]
            readList, _, _ = select.select(read, [], [], 1)

            # Receive from peers
            for socket in readList:
                peer = self.getPeerBySocket(socket)
                try:
                    msg = socket.recv(1024)
                except:
                    self.removePeer(peer)
                    continue

                if len(msg) == 0:
                    self.removePeer(peer)
                    continue

                peer.readBuffer += msg
                self.manageMessageReceived(peer)

    def startConnectionToPeers(self):
        for peer in self.peers:
            if not peer.hasHandshaked:
                try:
                    peer.sendToPeer(peer.handshake)
                    interested = peer.build_interested()
                    peer.sendToPeer(interested)
                except:
                    self.removePeer(peer)

    def manageMessageReceived(self, peer):
        while len(peer.readBuffer) > 0:
            if peer.hasHandshaked == False:
                peer.checkHandshake(peer.readBuffer)
                return

            msgLength = utils.convertBytesToDecimal(peer.readBuffer[0:4])

            # handle keep alive
            if peer.keep_alive(peer.readBuffer):
                return

            # len 0
            try:
                msgCode = int(ord(peer.readBuffer[4:5]))
                payload = peer.readBuffer[5:4 + msgLength]
            except Exception as e:
                logging.info(e)
                return

            # Message is not complete. Return
            if len(payload) < msgLength - 1:
                return

            peer.readBuffer = peer.readBuffer[msgLength + 4:]

            try:
                peer.idFunction[msgCode](payload)
            except Exception, e:
                logging.debug("error id:", msgCode, " ->", e)
                return


